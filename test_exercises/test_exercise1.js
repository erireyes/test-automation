const assert = require('assert')

describe('test_exercise1', () => {
    it('should buy small shirts', () => {
        browser.url('https://react-shopping-cart-67954.firebaseapp.com');
        browser.maximizeWindow();
        
        // Check page title
        var title = browser.getTitle();
        assert.strictEqual(title, 'React Shopping Cart');

        // Filter small products
        var small = $('div.filters > div:nth-child(3)');
        small.click();

        // Select Cat Tee Black T-Shirt
        var addSmallShirt1 = $('/html/body/div/main/div[2]/div[2]/div[4]');
        addSmallShirt1.click();

        browser.pause(2000);

        // Select Monkey Black T-Shirt
        var addSmallShirt2 = $('/html/body/div/main/div[2]/div[3]/div[4]');
        addSmallShirt2.click();

        // Click checkout button
        var checkOUt = $('div.float-cart__content > div.float-cart__footer > div.buy-btn');
        checkOUt.click();

        browser.pause(2000);

        browser.acceptAlert();

        browser.pause(2000);
    })

    it('should buy lowest price', () => {
        browser.reloadSession();
        browser.url('https://react-shopping-cart-67954.firebaseapp.com');
        browser.maximizeWindow();

        // Filter lowest price
        $("div.shelf-container-header > div > select").selectByVisibleText("Lowest to highest");

        browser.pause(2000);

        // Select cheapest shirt
        var cheapestShirt = $('/html/body/div/main/div[2]/div[2]/div[4]');
        cheapestShirt.click();

        // Click checkout button
        var checkOUt = $('div.float-cart__content > div.float-cart__footer > div.buy-btn');
        checkOUt.click();
         
        browser.pause(2000);

        browser.acceptAlert();

        browser.pause(2000);
    }
    )
})

